Instructions for inserting this pack into Frets on Fire, simply open with WinRAR and double click inside the Song Pack folder, then press the extract to button... choose to extract into your Frets on Fire/data/songs/ folder

Then click OK.

When it is done just close the WinRAR window then open the game.